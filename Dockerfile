FROM node:16-alpine as build

ENV NPM_CONFIG_LOGLEVEL warn

COPY . .
RUN npm install
RUN npm run build --producation

FROM node:16-alpine

ENV NPM_CONFIG_LOGLEVEL warn

RUN npm install -g serve@11

COPY --from=build ./build ./html

EXPOSE 5000

CMD [ "serve", "html" ]
