export const primary = '#18ae0c';
export const primaryLighter = '#99f2b2';

export const background = '#212529';

export const white = '#ffffff';
