import styled from "@emotion/styled";
import {white} from "../colors";

const Text = styled.div`
  color: ${white};
  line-height: 1.5;
`;

export default Text;
